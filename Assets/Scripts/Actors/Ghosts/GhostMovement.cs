﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GhostStates
{
    SCATTER,
    CHASE,
    TURN_TO_BLUE,
    EATEN
}

public class GhostMovement : GridMovement
{
    /// <summary>
    /// Stores the path found by the A* algorithm.
    /// </summary>
    private List<Cell> path;

    //Fetches the player just once and uses a stored reference the next time.
    private PlayerMovement pacman;

    /// <summary>
    /// Determines this ghost's appearance and strategy.
    /// </summary>
    private GhostType ghostType;

    /// <summary>
    /// Stores information on its own scatter spot cell.
    /// </summary>
    Cell scatterCell = null;

    /// <summary>
    /// Stores information on what target the ghost is currently walking towards.
    /// </summary>
    Cell pathTarget = null;

    /// <summary>
    /// This ghost's current state.
    /// </summary>
    /// <param name="type"></param>
    /// <param name="scatter"></param>
    public GhostStates state = GhostStates.SCATTER;

    public SpriteRenderer bodySprite, eyeSprite;
    public Sprite normalEyes, scared;

    public void OnEnable()
    {
        OnObjectReset += Recover;
    }

    public void OnDisable()
    {
        OnObjectReset -= Recover;
    }

    public void Initialize(GhostType type, Cell scatter)
    {
        ghostType = type;
        bodySprite.color = ghostType.color;

        //In cases of maps without scatter spots, we consider the ghost spawn as its scatter spot.
        if (scatter != null) scatterCell = scatter;
        else scatterCell = startingCell;
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if (!canMove) return;

        switch (state)
        {
            case GhostStates.SCATTER:
                if (ghostType.strategy == GhostStrategy.DONT_SCATTER)
                {
                    state = GhostStates.CHASE;
                    break;
                }
                else
                {
                    if (pathTarget != scatterCell) pathTarget = scatterCell;
                }
                break;
            case GhostStates.TURN_TO_BLUE:
                if (ghostType.strategy == GhostStrategy.FEARLESS)
                {
                    state = GhostStates.CHASE;
                    break;
                }
                else
                {
                    if (pathTarget != scatterCell) pathTarget = scatterCell;
                }
                break;
            case GhostStates.CHASE:
                if (ghostType.strategy == GhostStrategy.DONT_CHASE)
                {
                    state = GhostStates.SCATTER;
                    break;
                }
                else
                {
                    if (pathTarget != ChaseCell()) pathTarget = ChaseCell();
                }
                break;
            case GhostStates.EATEN:
            default:
                if (pathTarget != startingCell) pathTarget = startingCell;
                break;
        }

        ///Checks if the ghost is close enough to its own cell's center.
        ///This ensures we don't make path calculations too often and lag the game.
        if (MazeManager.Instance.CloseToCell(transform.position, currentCell, GlobalVariables.cellCenterThreshold * 1.5f)) UpdatePath(pathTarget);
    }

    /// <summary>
    /// Calculates which tile the ghost should chase. Each personality has a different one.
    /// </summary>
    /// <param name="pactile"></param>
    /// <returns></returns>
    private Cell ChaseCell()
    {
        if (pacman == null) pacman = FindObjectOfType<PlayerMovement>();

        Cell pacCell = MazeManager.Instance.GetCellFromPos(pacman.transform.position);
        Directions pacDirection = pacman.CurrentDirection;
        float distToPacman = Vector2.Distance(pacman.transform.position, transform.position);

        switch (ghostType.strategy)
        {
            case GhostStrategy.PINKY:
                return SearchAvailableCell(pacCell, pacman.CurrentDirection, 4);
            case GhostStrategy.INKY:
                return SearchAvailableCell(pacCell, pacman.CurrentDirection.OppositeDirection(), 4);
            case GhostStrategy.CLYDE:
                return distToPacman <= MazeManager.Instance.spacing * 4 ? scatterCell : pacCell;
            case GhostStrategy.BLINKY:
            default:
                return pacCell;
        }
    }

    private Cell SearchAvailableCell(Cell pacCell, Directions searchDirection, int initialDist)
    {
        Cell bestCell = null;
        int currentDist = initialDist;

        //Loops attempting to find an available cell closer to PAC-MAN's until only his own cell's left.
        while (bestCell == null || bestCell.content == CellContents.WALL || bestCell.content == CellContents.GHOST_WALL || bestCell.content == CellContents.GHOST_DOOR)
        {
            switch (searchDirection)
            {
                case Directions.UP:
                    bestCell = MazeManager.Instance.GetCellFromCoordinates(pacCell.coordinates.x, pacCell.coordinates.y - currentDist, false);
                    break;
                case Directions.DOWN:
                    bestCell = MazeManager.Instance.GetCellFromCoordinates(pacCell.coordinates.x, pacCell.coordinates.y + currentDist, false);
                    break;
                case Directions.LEFT:
                    bestCell = MazeManager.Instance.GetCellFromCoordinates(pacCell.coordinates.x - currentDist, pacCell.coordinates.y, false);
                    break;
                case Directions.RIGHT:
                    bestCell = MazeManager.Instance.GetCellFromCoordinates(pacCell.coordinates.x + currentDist, pacCell.coordinates.y, false);
                    break;
                default:
                    bestCell = pacCell;
                    break;
            }

            currentDist--;

            if (currentDist <= 0) return pacCell;
        }

        return bestCell;
    }

    /// <summary>
    /// Updates the pathfinding list once the ghost reaches the middle of the next cell.
    /// </summary>
    private void UpdatePath(Cell targetCell)
    {
        if (targetCell == null) return;

        var thisCell = MazeManager.Instance.GetCellFromPos(transform.position);

        //"Back cell" refers to the cell that's directly behind the ghost attempting to pathfind.
        //We ignore this cell to give ghosts their classic behaviour where they can't make 180 turns unless when
        //entering the TURN-TO-BLUE state, as documented here: https://www.youtube.com/watch?v=ataGotQ7ir8
        //This only applies for ghosts that are actually moving.
        Cell backCell = GetBackCell();

        path = AStarPathfinding.FindPath(thisCell, targetCell, backCell);
        //In cases where the ghosts are already in the same spot as the target or can't find a path to it,
        //we make then go past through it by telling them to cycle back to their spawn point, albeit for just a few frames.
        if (path == null || path.Count <= 0)
        {
            path = AStarPathfinding.FindPath(thisCell, startingCell, backCell);
            pathTarget = startingCell;

            if (path == null || path.Count <= 0)
            {
                //This situation usually represents when a ghost reaches his home spot after being eaten, 
                //so we can use this opportunity to reset its state.
                CurrentDirection = Directions.STOPPED;
                Recover();
                Scatter();
                return;
            }
        }

        var nextCell = path[0];

        //Sets the ghost's movement direction based on his next cell from the path list.
        if (nextCell.coordinates.x > thisCell.coordinates.x) //Goes right
        {
            CurrentDirection = Directions.RIGHT;
        }
        else if (nextCell.coordinates.x < thisCell.coordinates.x) //Goes left
        {
            CurrentDirection = Directions.LEFT;
        }
        else if (nextCell.coordinates.y > thisCell.coordinates.y) //Goes down
        {
            CurrentDirection = Directions.DOWN;
        }
        else if (nextCell.coordinates.y < thisCell.coordinates.y) //Goes up
        {
            CurrentDirection = Directions.UP;
        }
    }

    /// <summary>
    /// Changes this ghost's attributes when it scatters.
    /// </summary>
    /// <param name="scatter"></param>
    public void Scatter()
    {
        if (state == GhostStates.TURN_TO_BLUE) Recover();
        state = ghostType.strategy == GhostStrategy.DONT_SCATTER ? GhostStates.CHASE : GhostStates.SCATTER;

        state = GhostStates.SCATTER;
        speedMultiplier = ghostType.scatterSpeedMultiplier;
    }

    /// <summary>
    /// Changes this ghost's appearance when TURN_TO_BLUE ends.
    /// </summary>
    public void Chase()
    {
        if (state == GhostStates.EATEN) return;
        if (state == GhostStates.TURN_TO_BLUE) Recover();

        state = ghostType.strategy == GhostStrategy.DONT_CHASE ? GhostStates.SCATTER : GhostStates.CHASE;
        speedMultiplier = ghostType.chaseSpeedMultiplier;
        CurrentDirection = Directions.STOPPED;
    }

    /// <summary>
    /// Changes this ghost's attributes when it gets scared.
    /// </summary>
    public void TurnToBlue()
    {
        if (state == GhostStates.TURN_TO_BLUE || state == GhostStates.EATEN) return;
        if (ghostType.strategy == GhostStrategy.FEARLESS) return;

        state = GhostStates.TURN_TO_BLUE;
        bodySprite.color = Color.blue;
        eyeSprite.sprite = scared;
        speedMultiplier = ghostType.frightSpeedMultiplier;
        CurrentDirection = Directions.STOPPED;
    }

    /// <summary>
    /// Changes the ghost's attributes when it gets eaten by PAC-MAN.
    /// </summary>
    public void GetEaten()
    {
        if (state == GhostStates.EATEN) return;

        state = GhostStates.EATEN;
        bodySprite.color = new Color(0, 0, 0, 0);
        eyeSprite.sprite = normalEyes;
        speedMultiplier = GlobalVariables.eatenSpeed;
        CurrentDirection = Directions.STOPPED;
    }

    public void Recover()
    {
        bodySprite.color = ghostType.strategy == GhostStrategy.RANDOM ? Color.gray : ghostType.color;
        eyeSprite.sprite = normalEyes;
    }

    /// <summary>
    /// Returns the cell directly behind the ghost
    /// </summary>
    /// <param name="backCell"></param>
    /// <returns></returns>
    private Cell GetBackCell()
    {
        switch (CurrentDirection)
        {
            case Directions.UP:
                return MazeManager.Instance.GetCellFromCoordinates(currentCell.coordinates.x, currentCell.coordinates.y + 1, true);
            case Directions.DOWN:
                return MazeManager.Instance.GetCellFromCoordinates(currentCell.coordinates.x, currentCell.coordinates.y - 1, true);
            case Directions.LEFT:
                return MazeManager.Instance.GetCellFromCoordinates(currentCell.coordinates.x + 1, currentCell.coordinates.y, true);
            case Directions.RIGHT:
                return MazeManager.Instance.GetCellFromCoordinates(currentCell.coordinates.x - 1, currentCell.coordinates.y, true);
            case Directions.STOPPED:
            default:
                return currentCell;
        }
    }

    protected override void CheckCollisions()
    {
        //Do Nothing.
    }

    /// <summary>
    /// Draws a debug path of what path this ghost is taking.
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = ghostType.strategy == GhostStrategy.RANDOM ? Color.gray : ghostType.color;
        if (path != null)
        {
            for (int i = 0; i < path.Count; i++)
            {
                if (i + 1 >= path.Count) break;
                Gizmos.DrawLine(path[i].Position(), path[i + 1].Position());
            }
        }
    }
}