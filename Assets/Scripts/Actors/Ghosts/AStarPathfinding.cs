﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MazeManager;

/// <summary>
/// This class implements the A* algorithm using data that is already
/// ready-to-use from MazeGenerator, such as the game grid, cell positions and contents.
/// This is obviously unoptimized and sometimes lags the game (such as when putting a target off-bounds) 
/// but I didn't have time to create a Heap for it!
/// </summary>
public static class AStarPathfinding
{
    public static List<Cell> FindPath(Cell startCell, Cell targetCell, Cell backCell = null)
    {
        ///A list of cells that can be explored to find a path.
        List<Cell> openSet = new List<Cell>();

        ///A hashset of cells that have been already explored.
        ///Since we can't have duplicate cells on it, we use a HashSet instead of a List.
        HashSet<Cell> closedSet = new HashSet<Cell>();

        openSet.Add(startCell);
        if (backCell != null) closedSet.Add(backCell);

        while (openSet.Count > 0)
        {
            //Finds a open node with the lowest F-Cost.
            Cell currentCell = openSet[0];
            for (int i = 1; i < openSet.Count; i++) //Skips index 0 because it's the current cell.
            {
                if (openSet[i].path.fCost < currentCell.path.fCost
                    || (openSet[i].path.fCost == currentCell.path.fCost && openSet[i].path.hCost < currentCell.path.hCost))
                {
                    currentCell = openSet[i];
                }
            }

            openSet.Remove(currentCell);
            closedSet.Add(currentCell);

            if (currentCell == targetCell)
            {
                //Path found.
                return RetracePath(startCell, targetCell);
            }
            else
            {
                //Path not found yet. Loop through neighbors. Excludes neighbors with the "WALL" content.
                foreach (Cell ne in MazeManager.Instance.GetCellNeighbors(currentCell, new HashSet<CellContents> { CellContents.WALL, CellContents.GHOST_WALL }))
                {
                    //Skips if the neighboring cell has already been calculated.
                    if (closedSet.Contains(ne))
                    {
                        continue;
                    }

                    //Checks if a new neighbor with less cost has appeared;
                    var newCost = currentCell.path.gCost + MazeManager.Instance.GetCellDistance(currentCell, ne);

                    if (newCost < ne.path.gCost || !openSet.Contains(ne))
                    {
                        ne.path.gCost = newCost;
                        ne.path.hCost = MazeManager.Instance.GetCellDistance(ne, targetCell);
                        ne.path.parent = currentCell;

                        if (!openSet.Contains(ne))
                        {
                            openSet.Add(ne);
                        }
                    }
                }
            }
        }

        return null;
    }

    /// <summary>
    /// Traces backwards the path taken to find the shortest distance by using the
    /// cell's parent references.
    /// </summary>
    /// <param name="startCell"></param>
    /// <param name="endCell"></param>
    private static List<Cell> RetracePath(Cell startCell, Cell endCell)
    {
        List<Cell> path = new List<Cell>();
        Cell currentCell = endCell;

        while (currentCell != startCell)
        {
            path.Add(currentCell);
            currentCell = currentCell.path.parent;
        }

        //Finally, reverts the order of the path list to have them arranged by distance
        //from the closest to the farthest;
        path.Reverse();

        return path;
    }
}
