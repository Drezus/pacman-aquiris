﻿using System;
using UnityEngine;
/// <summary>
/// A class representing a type of ghost.
/// </summary>
[Serializable]
public class GhostType
{
    public string name;
    public Color color = Color.white;
    public GhostStrategy strategy;
    public float chaseSpeedMultiplier = 0.8f;
    public float scatterSpeedMultiplier = 0.8f;
    public float frightSpeedMultiplier = 0.8f;
}

public enum GhostStrategy
{
    BLINKY, //Chases PAC-MAN's tile during CHASE phases.
    PINKY, //Chases up to 4 tiles in front of PAC-MAN.
    INKY, //Chases up to 4 tiles behind PAC-MAN, achieving a flank effect.
    CLYDE, //Chases PAC-MAN's tile until he's 4 tiles from PAC-MAN, in which case it'll instead target its scatter tile.
    DONT_CHASE, //Chases its scatter tile only.
    DONT_SCATTER, //Chases PAC-MAN's tile only.
    FEARLESS, //Chases PAC-MAN's tile and won't be TURNED-TO-BLUE when a Power-Pellet is eaten (Mega-Pellets are still effective).
    RANDOM //Randomizes between any of the previous personalities every time they revive from TURN-TO-BLUE.
}