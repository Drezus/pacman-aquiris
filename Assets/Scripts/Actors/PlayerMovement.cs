﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : GridMovement
{
    /// <summary>
    /// The player's next direction to take when crossing a valid grid.
    /// This acts like a buffer, so you can input a direction long before actually turning,
    /// just like in the original arcade game.
    /// </summary>
    protected Directions inputDirection = Directions.STOPPED;

    //Called by the player everytime PAC-MAN collects a pellet.
    public delegate void CollectEvent(CellContents pelletType);
    public CollectEvent OnPelletCollected;

    //Called by the player everytime PAC-MAN hits a ghost without being powered-up.
    public delegate void LoseLifeEvent();
    public LoseLifeEvent OnLifeLost;

    //Called by the player everytime PAC-MAN hits a ghost with a power-up.
    public delegate void EatEvent(GhostMovement gho);
    public EatEvent OnGhostEaten;

    [HideInInspector]
    public bool megaPacman = false;

    public Transform sprite;
    public Animator anim;

    public void OnEnable()
    {
        speedMultiplier = 1f;
        OnCellChanged += InteractWithCell;
        OnPelletCollected += GameManager.Instance.PelletCollected;
        OnLifeLost += GameManager.Instance.LoseLife;
        OnGhostEaten += GameManager.Instance.GhostEaten;
        OnObjectReset += ClearSpawn;
    }

    public void OnDisable()
    {
        OnCellChanged -= InteractWithCell;
        OnPelletCollected -= GameManager.Instance.PelletCollected;
        OnLifeLost -= GameManager.Instance.LoseLife;
        OnGhostEaten -= GameManager.Instance.GhostEaten;
        OnObjectReset -= ClearSpawn;
    }

    private void Update()
    {
        RegisterInputs();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        AttemptDirectionChange();
        CheckCollisions();
    }

    private void LateUpdate()
    {
        ///Sets the anim speed
        if (CurrentDirection == Directions.STOPPED || !canMove) anim.speed = 0f;
        else anim.speed = 1f;
    }

    /// <summary>
    /// Acts like a collision detection, but it is much simpler implemented, without even relying on distance calculations.
    /// The method just compares what's in the player's current tile and interacts with it.
    /// </summary>
    private void InteractWithCell()
    {
        //No need to interact with it too if it's empty.
        if (currentCell.content == CellContents.EMPTY || currentCell.content == CellContents.PAC_SPAWN || currentCell.content == CellContents.GHOST_SPAWN) return;

        switch (currentCell.content)
        {
            case CellContents.PELLET:
            case CellContents.POWER_PELLET:
            case CellContents.MEGA_PELLET:
                //We collected a pellet. Play a sound, erase it from the cell and get points.
                OnPelletCollected?.Invoke(currentCell.content);
                DestroyCell(currentCell);
                break;
            default:
                //Do nothing.
                break;
        }
    }

    private void DestroyCell(Cell ce)
    {
        ce.content = CellContents.EMPTY;
        Destroy(ce.contentGO);
    }

    /// <summary>
    /// Uses the buffered input direction to try and turn the player if there's a valid path for that.
    /// We wait until the player is close enough to a cell's origin to attempt a turn, but like in the original game,
    /// there's a few units of tolerance to give players an edge over pursuing ghosts, as documented here: 
    /// https://www.gamasutra.com/view/feature/132330/the_pacman_dossier.php?page=4 
    /// </summary>
    private void AttemptDirectionChange()
    {
        //If no direction is buffered, or the buffered direction is the same as the current motion, then we can just ignore this.
        if (inputDirection == Directions.STOPPED || inputDirection == CurrentDirection) return;

        //We check if there's actually a valid path towards the intended direction using the coordinates from our own cell.
        Cell targetCell = null;
        switch (inputDirection)
        {
            case Directions.UP:
                targetCell = MazeManager.Instance.GetCellFromCoordinates(currentCell.coordinates.x, currentCell.coordinates.y - 1, true);
                break;
            case Directions.DOWN:
                targetCell = MazeManager.Instance.GetCellFromCoordinates(currentCell.coordinates.x, currentCell.coordinates.y + 1, true);
                break;
            case Directions.LEFT:
                targetCell = MazeManager.Instance.GetCellFromCoordinates(currentCell.coordinates.x - 1, currentCell.coordinates.y, true);
                break;
            case Directions.RIGHT:
                targetCell = MazeManager.Instance.GetCellFromCoordinates(currentCell.coordinates.x + 1, currentCell.coordinates.y, true);
                break;
            default:
                return;
        }
        //A valid direction means that it isn't a wall or a ghost-door.
        //If any of those apply, we can just ignore the direction change attempt and keep moving forward.
        if (targetCell.content == CellContents.WALL || targetCell.content == CellContents.GHOST_DOOR) return;

        //Finally, we prepare to actually input the turn.
        //In cases where the player is reversing his direction or is completely stopped, we won't wait to reach a cell's origin in order to 
        //simulate the original game's behaviour as close as possible.
        if (CurrentDirection.IsOppositeOf(inputDirection) || CurrentDirection == Directions.STOPPED)
        {
            ChangeDirection();
            targetCell = null;
        }
        else
        {
            //If the directions aren't opposites, we check if the player is close enough to its own cell's center to initiate a turn attempt.
            if (MazeManager.Instance.CloseToCell(transform.localPosition, currentCell, GlobalVariables.cellCenterThreshold))
            {
                ChangeDirection();
                targetCell = null;
            }
        }
    }

    private void ChangeDirection()
    {
        CurrentDirection = inputDirection;
        inputDirection = Directions.STOPPED;

        ///We use this opportunity to change the sprite's direction too.
        switch (CurrentDirection)
        {
            case Directions.UP:
                sprite.localEulerAngles = new Vector3(0f, 0f, 90f);
                break;
            case Directions.DOWN:
                sprite.localEulerAngles = new Vector3(0f, 0f, -90f);
                break;
            case Directions.LEFT:
                sprite.localEulerAngles = new Vector3(0f, -180f, 0f);
                break;
            case Directions.RIGHT:
                sprite.localEulerAngles = Vector3.zero;
                break;
        }
    }

    protected override void CheckCollisions()
    {
        //No need to make detections if the game isn't playing.
        if (GameManager.Instance.currentGameState != GameStates.PLAYING) return;

        //No need to make collision detections with walls if the player is stopped.
        if (CurrentDirection != Directions.STOPPED)
        {
            Cell nextCell = null;
            switch (CurrentDirection)
            {
                case Directions.UP:
                    nextCell = MazeManager.Instance.GetCellFromCoordinates(currentCell.coordinates.x, currentCell.coordinates.y - 1, true);
                    break;
                case Directions.DOWN:
                    nextCell = MazeManager.Instance.GetCellFromCoordinates(currentCell.coordinates.x, currentCell.coordinates.y + 1, true);
                    break;
                case Directions.LEFT:
                    nextCell = MazeManager.Instance.GetCellFromCoordinates(currentCell.coordinates.x - 1, currentCell.coordinates.y, true);
                    break;
                case Directions.RIGHT:
                    nextCell = MazeManager.Instance.GetCellFromCoordinates(currentCell.coordinates.x + 1, currentCell.coordinates.y, true);
                    break;
                default:
                    return;
            }

            //In the unlikely event that there's no next cell to walk to, we just cut the code short.
            if (nextCell == null) return;

            //Determines what to do based on what's the content of the next cell.
            //There's always a distance check to confirm the player is next enough to collide with the object.
            switch (nextCell.content)
            {
                case CellContents.WALL:
                case CellContents.GHOST_DOOR:
                case CellContents.GHOST_WALL:
                    //This accounts for both cases when PAC-MAN is very close to a wall or very far (like when warping the screen).
                    //I know it's a hacky solution, but I don't have much time left now!
                    if (Vector2.Distance(transform.position, nextCell.Position()) < MazeManager.Instance.spacing || Vector2.Distance(transform.position, nextCell.Position()) > 5f)
                    {
                        //We hit a wall. Just set direction to "stopped" and the player will freeze in place.
                        CurrentDirection = Directions.STOPPED;
                    }
                    break;
            }
        }

        foreach (GhostMovement gho in GameManager.Instance.spawnedGhosts)
        {
            if (gho.state == GhostStates.EATEN) return;

            //The minimun distance to collide with ghosts changes when PAC-MAN is mega.
            var minDist = megaPacman ? MazeManager.Instance.spacing * 1.5f : MazeManager.Instance.spacing * 0.8f;

            if (Vector2.Distance(transform.position, gho.transform.position) <= minDist)
            {
                //Eats the ghost. Mega-PAC-MAN can eat even regular ghosts!
                if (gho.state == GhostStates.TURN_TO_BLUE || megaPacman)
                {
                    OnGhostEaten?.Invoke(gho);
                }
                else
                {
                    //Dies by the ghost
                    CurrentDirection = Directions.STOPPED;
                    inputDirection = Directions.STOPPED;
                    OnLifeLost?.Invoke();
                }
            }
        }
    }

    private void RegisterInputs()
    {
        if (GameManager.Instance.currentGameState != GameStates.PLAYING) return;

        if (Input.GetAxis("Vertical") > 0)
        {
            inputDirection = Directions.UP;
        }
        else if (Input.GetAxis("Vertical") < 0)
        {
            inputDirection = Directions.DOWN;
        }

        if (Input.GetAxis("Horizontal") > 0)
        {
            inputDirection = Directions.RIGHT;
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            inputDirection = Directions.LEFT;
        }
    }

    public void BecomeMega()
    {
        megaPacman = true;
        transform.localScale = new Vector3(2.8f, 2.8f, 2.8f);
        OnCellChanged += DestroySurroundings;

        //Plays the Mega sound and just them attempts to destroy surrounding neighbors.
        StartCoroutine(GameManager.Instance.PauseEffect(1f, Sounds.MEGA_PELLET_EATEN, () => DestroySurroundings()));
    }

    public void RevertToRegular()
    {
        if (!megaPacman) return;
        megaPacman = false;
        transform.localScale = new Vector3(1, 1, 1);

        StartCoroutine(GameManager.Instance.PauseEffect(1f, Sounds.MEGA_PELLET_OVER));

        OnCellChanged -= DestroySurroundings;
    }

    private void DestroySurroundings()
    {
        var surroundings = MazeManager.Instance.GetCellNeighbors(currentCell);

        //This limits the time we play the destroy sound, so it only plays once
        //for every group of destroyed walls.
        bool wallDestroyed = false;

        foreach (Cell sur in surroundings)
        {
            switch (sur.content)
            {
                case CellContents.WALL:
                    wallDestroyed = true;
                    DestroyCell(sur);
                    break;
                case CellContents.PELLET:
                case CellContents.POWER_PELLET:
                case CellContents.MEGA_PELLET:
                    //We collected a pellet. Play a sound, erase it from the cell and get points.
                    OnPelletCollected?.Invoke(sur.content);
                    DestroyCell(sur);
                    break;
                default:
                    //Do nothing.
                    break;
            }
        }

        if (wallDestroyed) AudioManager.Instance.PlaySound(Sounds.DESTROY);
    }

    private void ClearSpawn()
    {
        if (currentCell.content != CellContents.PAC_SPAWN)
        {
            DestroyCell(currentCell);
            currentCell.content = CellContents.PAC_SPAWN;
        }
    }
}
