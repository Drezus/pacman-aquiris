﻿using UnityEngine;

/// <summary>
/// States that determine which direction the instance is moving.
/// This also helps to avoid situations with "overlapping" directions.
/// </summary>
public enum Directions
{
    STOPPED = 0,
    UP = 1,
    DOWN = 2,
    LEFT = 3,
    RIGHT = 4
}

/// <summary>
/// A overall movement class that makes both Pac-Man and the Ghost 
/// </summary>
public abstract class GridMovement : MonoBehaviour
{
    /// <summary>
    /// Fires an event when this object's position is reset.
    /// </summary>
    public delegate void ResetEvent();
    public ResetEvent OnObjectReset;

    /// <summary>
    /// Character speeds are determined as multipliers from the player's (pac-man's) own speed (100%).
    /// </summary>
    public float speedMultiplier = 1f;

    /// <summary>
    /// Stores instance's current direction state.
    /// </summary>
    private Directions currentDirection = Directions.STOPPED;

    /// <summary>
    /// Public accessor to currentDirection. We use this to benefit from a realign of the sprite to the grid everytime a direction is changed,
    /// by repositioning it to the corresponding collum or row.
    /// This makes sure sprites won't veer off too much of the game's intended grid look.
    /// </summary>
    public Directions CurrentDirection
    {
        get
        {
            return currentDirection;
        }
        protected set
        {
            currentDirection = value;

            //When changing directions, we make sure the player is aligned to 
            if (currentDirection == Directions.UP || currentDirection == Directions.DOWN || currentDirection == Directions.STOPPED)
            {
                transform.position = new Vector2(MazeManager.Instance.GetXFromCol(currentCell.coordinates.x), transform.position.y);
            }

            if (currentDirection == Directions.LEFT || currentDirection == Directions.RIGHT || currentDirection == Directions.STOPPED)
            {
                transform.position = new Vector2(transform.position.x, MazeManager.Instance.GetYFromRow(currentCell.coordinates.y));
            }
        }
    }

    /// <summary>
    /// Determines if the instance may move or not.
    /// </summary>
    protected bool canMove { get { return GameManager.Instance.currentGameState == GameStates.PLAYING; } }

    /// <summary>
    /// Gets the cell that this object is currently closest from.
    /// </summary>
    protected Cell currentCell { get { return MazeManager.Instance.GetCellFromPos(transform.position); } }

    /// <summary>
    /// Stores the cell where it began.
    /// </summary>
    protected Cell startingCell = null;

    /// <summary>
    /// Stores which was the last visited cell.
    /// </summary>
    protected Cell lastCell = null;

    /// <summary>
    /// Events that trigger when a new cell is visited.
    /// </summary>
    /// <param name="ce"></param>
    public delegate void CellChangeEvent();
    public CellChangeEvent OnCellChanged;

    public void Start()
    {
        startingCell = currentCell;
    }

    protected virtual void FixedUpdate()
    {
        if (!canMove || currentDirection == Directions.STOPPED) return;

        Move();
        WrapAround();
        CheckCollisions();

        //Fires an event every time this instance switches cell.
        if (lastCell != currentCell)
        {
            OnCellChanged?.Invoke();
            lastCell = currentCell;
        }
    }

    /// <summary>
    /// Checks for any obstacle within maze-spacing units from the player.
    /// I decided not to use physics-based collisions (even with Box2D) because this feels like overkill for a simple game like this.
    /// Instead, we just check how close other objects might be from the player.
    /// </summary>
    protected abstract void CheckCollisions();

    private void Move()
    {
        if (!canMove) return;

        switch (currentDirection)
        {
            case Directions.UP:
                transform.Translate(Vector3.up * GlobalVariables.baseSpeed * (Time.deltaTime * speedMultiplier));
                break;
            case Directions.DOWN:
                transform.Translate(Vector3.down * GlobalVariables.baseSpeed * (Time.deltaTime * speedMultiplier));
                break;
            case Directions.LEFT:
                transform.Translate(Vector3.left * GlobalVariables.baseSpeed * (Time.deltaTime * speedMultiplier));
                break;
            case Directions.RIGHT:
                transform.Translate(Vector3.right * GlobalVariables.baseSpeed * (Time.deltaTime * speedMultiplier));
                break;
        }
    }

    private void WrapAround()
    {
        var mazeGen = MazeManager.Instance;

        //Wrapping around the screen horizontally
        if (transform.localPosition.x > mazeGen.mazeWidth)
        {
            transform.localPosition = new Vector2(0, transform.localPosition.y);
        }
        else if (transform.localPosition.x < 0)
        {
            transform.localPosition = new Vector2(mazeGen.mazeWidth, transform.localPosition.y);
        }

        //Wrapping around the screen vertically.
        //Values are negative because the maze is generated below Unity's Y origin.
        if (transform.localPosition.y < (mazeGen.mazeHeight * -1))
        {
            transform.localPosition = new Vector2(transform.localPosition.x, 0);
        }
        else if (transform.localPosition.y > 0)
        {
            transform.localPosition = new Vector2(transform.localPosition.x, -mazeGen.mazeHeight);
        }
    }

    public void ResetPosition()
    {
        transform.SetPositionAndRotation(startingCell.Position(), Quaternion.identity);
        OnObjectReset?.Invoke();
    }
}