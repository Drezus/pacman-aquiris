﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Maze Cell was originally a Struct, but a change to Class was necessary in order to be able to modify its contents during a foreach iteration.
/// </summary>
public class Cell
{
    /// <summary>
    /// Stores information on what collumn and row the cell resides.
    /// </summary>
    public Vector2Int coordinates;

    /// <summary>
    /// Informs what kind of tile this cell is.
    /// It's useful for collision detection and pathfinding in case of wall tiles.
    /// </summary>
    public CellContents content;

    /// <summary>
    /// If the cell houses a collectible such as a Pellet, then we can keep track of it
    /// with this reference and destroy when necessary.
    /// </summary>
    public GameObject contentGO;

    /// <summary>
    /// Stores pathfinding cost information to be used by the A* algorithm.
    /// </summary>
    public PathCosts path;

    /// <summary>
    /// Property that returns this cell's transform position in Unity space.
    /// </summary>
    /// <param name="ce"></param>
    /// <returns></returns>
    public Vector2 Position()
    {
        return new Vector2(coordinates.x * MazeManager.Instance.spacing, coordinates.y * -MazeManager.Instance.spacing);
    }
}

#region Cell Structs
public struct PathCosts
{
    /// <summary>
    /// Represents the movement cost of going from the starting cell to this one.
    /// </summary>
    public int gCost;
    /// <summary>
    /// Represents the Heuristic cost of this cell, with an estimation of the distance to the target.
    /// </summary>
    public int hCost;

    /// <summary>
    /// The Final Cost, which is simply gCost + hCost.
    /// </summary>
    public int fCost { get { return gCost + hCost; } }

    /// <summary>
    /// In pathfinding terms, this represents the cell that was been traversed before reaching this one.
    /// </summary>
    public Cell parent;
}

public enum CellContents
{
    EMPTY,
    WALL,
    PELLET,
    POWER_PELLET,
    GHOST_DOOR,
    PAC_SPAWN,
    GHOST_SPAWN,
    MEGA_PELLET,
    GHOST_WALL,
}

class CellNotFoundException : Exception
{
    public CellNotFoundException()
    {
    }

    public CellNotFoundException(string message)
        : base(message)
    {
    }

    public CellNotFoundException(string message, Exception inner)
        : base(message, inner)
    {
    }
}
#endregion
