﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Class that handles both maze generation and operations.
/// Using a singleton on it may seem overkill, but this makes more convenient for Player/Enemies to use
/// helper functions without requiring direct reference to this monobehaviour.
/// </summary>
public class MazeManager : Singleton<MazeManager>
{
    public Cell[,] maze;
    public Vector2Int gridSize = new Vector2Int(28, 36); //Defines the map's cell count in rows and collumns.
    public float spacing = 0.13f; //Defines how much close apart cell sprites will be when spawned.

    /// <summary>
    /// Spawnable Prefabs
    /// </summary>
    [Header("Procedurally-spawned Prefabs")]
    public GameObject pacmanPrefab;
    public GameObject ghostPrefab;
    public GameObject wallPrefab;
    public GameObject ghostDoorPrefab;
    public GameObject pelletPrefab;
    public GameObject powerPelletPrefab;
    public GameObject megaPelletPrefab;

    /// <summary>
    /// Store pellet gameobjects.
    /// </summary>
    public Transform pelletsParent;

    [HideInInspector]
    public int pelletsParsed = 0;

    /// <summary>
    /// Keeps track of PAC-MAN's spawn to create a mega-pellet there when needed.
    /// </summary>
    private Cell megaPelletSpawn;

    /// <summary>
    /// Keeps track of all scatter spots parsed to assign them to ghosts aftwards.
    /// </summary>
    [HideInInspector]
    public List<Cell> scatterSpots;

    public void CreateMaze(int mazeID)
    {
        maze = ParseMap(GlobalVariables.maps[mazeID]);
        GenerateMaze(maze);
    }

    #region Maze Generation
    /// <summary>
    /// Converts a given string to a functional level.
    /// I decided not to use regular editor tools for building levels both because this allows for
    /// dynamically loading levels (such as via an external .txt file) and because it enables more flexibility
    /// with collisions without depending on costly features such as Box2D or Physics.
    /// </summary>
    /// <param name="mapString"></param>
    /// <returns></returns>
    private Cell[,] ParseMap(string mapString)
    {
        char[] charArray = mapString.ToUpperInvariant().ToCharArray();

        Cell[,] grid = new Cell[gridSize.x, gridSize.y]; //Creates the grid according to the size parameters.

        int pacSpawnsParsed = 0;
        pelletsParsed = 0;

        scatterSpots = new List<Cell>();

        for (int row = 0; row < gridSize.x; row++)
        {
            for (int col = 0; col < gridSize.y; col++)
            {
                Cell thisCell = new Cell();
                thisCell.coordinates = new Vector2Int(row, col);

                int currentChar = (col * gridSize.x) + row;

                //Failsafe in case the provided string map isn't big enough to fill the cells.
                if (currentChar >= charArray.Length)
                {
                    thisCell.content = CellContents.EMPTY;
                }
                else
                {
                    char contentChar = charArray[currentChar];

                    /// Captions:
                    /// E = Empty
                    /// W = Wall
                    /// P = PAC-MAN's spawn point. Also serves as a bonus fruit spawn point, when needed.
                    /// G = Ghost spawn points. One ghost spawns for each.
                    /// D = Ghost-door. Walls that ghosts can go through, but PAC-MAN can't.
                    /// . = Regular pellet.
                    /// O = Power-Pellet. 
                    /// S = Ghost scatter spot. Ghosts share the same spot if the map has more Ghosts than Scatter spots.
                    /// Z = Ghost scatter spot with a pellet on it.
                    /// X = Ghost house's walls. They can't be destroyed even by MEGA-PAC-MAN.

                    switch (contentChar)
                    {
                        case 'W':
                            thisCell.content = CellContents.WALL;
                            break;
                        case 'X':
                            thisCell.content = CellContents.GHOST_WALL;
                            break;
                        case 'P':
                            //Only counts one PAC-MAN spawn. If it was already accounted for, the other ones are instead
                            //ignored and become regular empty spaces.
                            thisCell.content = pacSpawnsParsed <= 0 ? CellContents.PAC_SPAWN : CellContents.EMPTY;

                            //We actually keep track of how many extra unused pac-spawns were used so we can inform the player
                            //that he has excessive spawns in his/her custom string map.
                            pacSpawnsParsed++;
                            break;
                        case 'G':
                            thisCell.content = CellContents.GHOST_SPAWN;
                            break;
                        case '.':
                            thisCell.content = CellContents.PELLET;
                            pelletsParsed++;
                            break;
                        case 'O':
                            thisCell.content = CellContents.POWER_PELLET;
                            pelletsParsed++;
                            break;
                        case 'D':
                            thisCell.content = CellContents.GHOST_DOOR;
                            break;
                        case 'S':
                            thisCell.content = CellContents.EMPTY;
                            scatterSpots.Add(thisCell);
                            break;
                        case 'Z':
                            thisCell.content = CellContents.PELLET;
                            scatterSpots.Add(thisCell);
                            pelletsParsed++;
                            break;
                        case 'E':
                        default:
                            thisCell.content = CellContents.EMPTY;
                            break;
                    }
                }
                grid[row, col] = thisCell;
            }
        }

        return grid;
    }

    private void GenerateMaze(Cell[,] cellMap)
    {
        ///Keeps track of how many ghosts were spawned to add diversity.
        int ghostsSpawned = 0;

        foreach (Cell ce in cellMap)
        {
            switch (ce.content)
            {
                case CellContents.PAC_SPAWN:
                    CreateInstanceOnCell(pacmanPrefab, ce);
                    megaPelletSpawn = ce;
                    break;
                case CellContents.GHOST_SPAWN:
                    if (ghostPrefab == null) return;

                    GhostMovement thisGhost = CreateInstanceOnCell(ghostPrefab, ce).GetComponent<GhostMovement>();
                    GameManager.Instance.spawnedGhosts.Add(thisGhost);
                    ghostsSpawned++;
                    break;
                case CellContents.WALL:
                    CreateInstanceOnCell(wallPrefab, ce, transform);
                    break;
                case CellContents.GHOST_DOOR:
                    CreateInstanceOnCell(ghostDoorPrefab, ce, transform);
                    break;
                case CellContents.GHOST_WALL:
                    var thisWall = CreateInstanceOnCell(wallPrefab, ce, transform);
                    if (thisWall != null) thisWall.GetComponent<SpriteRenderer>().color = Color.cyan;
                    break;
                case CellContents.PELLET:
                    CreateInstanceOnCell(pelletPrefab, ce, pelletsParent);
                    break;
                case CellContents.POWER_PELLET:
                    CreateInstanceOnCell(powerPelletPrefab, ce, pelletsParent);
                    break;
                case CellContents.EMPTY:
                default:
                    break;
            }
        }
    }

    private GameObject CreateInstanceOnCell(GameObject go, Cell ce, Transform parent = null)
    {
        if (go == null) return null;

        var thisGO = Instantiate(go);
        thisGO.transform.localPosition = ce.Position();
        ce.contentGO = thisGO;

        //For organization sake, this lets us store gameObject inside parents.
        thisGO.transform.SetParent(parent);

        return thisGO;
    }

    public void SpawnMegaPellet()
    {
        if (megaPelletSpawn.content == CellContents.MEGA_PELLET) return;

        CreateInstanceOnCell(megaPelletPrefab, megaPelletSpawn, pelletsParent);
        megaPelletSpawn.content = CellContents.MEGA_PELLET;
        AudioManager.Instance.PlaySound(Sounds.MEGA_PELLET_APPEAR);
    }
    #endregion

    #region Helper Methods
    //Property shortcuts to commonly-used calculations
    public float mazeWidth { get { return (gridSize.x - 1) * spacing; } }
    public float mazeHeight { get { return (gridSize.y - 1) * spacing; } }

    /// <summary>
    /// For a given position in Unity space, returns the cell that's closest to it.
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public Cell GetCellFromPos(Vector2 pos)
    {
        Cell closestCell = null;

        float closestDistance = Mathf.Infinity;

        foreach (Cell ce in maze)
        {
            float distanceToCell = Vector2.Distance(pos, new Vector2(ce.coordinates.x * spacing, ce.coordinates.y * -spacing));
            if (distanceToCell < closestDistance)
            {
                closestDistance = distanceToCell;
                closestCell = ce;
            }
        }

        if (closestCell != null) return closestCell;

        throw new CellNotFoundException("Couldn't find a corresponding cell close to position " + pos.x + "x " + pos.y + "y.");
    }

    /// <summary>
    /// For a given coordinate, returns the cell that corresponds to it.
    /// For coordinates that go beyond the scope of the maze, we just wrap them around.
    /// </summary>
    /// <param name="coord"></param>
    /// <returns></returns>
    public Cell GetCellFromCoordinates(int x, int y, bool wrapping)
    {
        //Wraps the coordinates around the maze's limits
        if (wrapping)
        {
            if (x >= gridSize.x) x = 0;
            if (x < 0) x = gridSize.x - 1;

            if (y >= gridSize.y) y = 0;
            if (y < 0) y = gridSize.y - 1;
        }


        foreach (Cell ce in maze)
        {
            //Comparisons between floating-point values tend to fail due to precision, so we introduce a safe threshold.
            if (Math.Abs(ce.coordinates.x - x) < 0.001 && Math.Abs(ce.coordinates.y - y) < 0.001) return ce;
        }

        //A null return is expected in cases where wrapping is disabled (such as during pathfinding, 
        //where the algorithm does not consider out of bounds cells).
        return null;
    }

    /// <summary>
    /// Determines if a position is sufficiently close to a cell's origin point using the tolerance threshold.
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="cell"></param>
    /// <returns></returns>
    public bool CloseToCell(Vector2 pos, Cell cell, float dist)
    {
        return Vector2.Distance(pos, cell.Position()) <= dist;
    }

    /// <summary>
    /// For a given collumn ID, gets the collumn's X position.
    /// This works even for collumns beyond the maze's bounds, which is useful for 
    /// wrapping around the screen.
    /// </summary>
    /// <param name="colID"></param>
    /// <returns></returns>
    public float GetXFromCol(int colID)
    {
        return (colID * spacing);
    }

    /// <summary>
    /// For a given row  ID, gets the row's Y position.
    /// This works even for rows beyond the maze's bounds, which is useful for 
    /// wrapping around the screen.
    /// </summary>
    /// <param name="colID"></param>
    /// <returns></returns>
    public float GetYFromRow(int rowID)
    {
        return (rowID * -spacing);
    }

    /// <summary>
    /// For a given cell, returns a list of their surrounding neighboring cells.
    /// </summary>
    /// <param name="ce"></param>
    /// <returns></returns>
    public List<Cell> GetCellNeighbors(Cell ce, HashSet<CellContents> excludeContent = null)
    {
        List<Cell> neighbors = new List<Cell>();

        //Add top cell
        var topCell = GetCellFromCoordinates(ce.coordinates.x, ce.coordinates.y + 1, true);
        if (topCell != null && excludeContent == null || (excludeContent != null && !excludeContent.Contains(topCell.content))) neighbors.Add(topCell);

        //Add left cell
        var leftCell = GetCellFromCoordinates(ce.coordinates.x - 1, ce.coordinates.y, true);
        if (leftCell != null && excludeContent == null || (excludeContent != null && !excludeContent.Contains(leftCell.content))) neighbors.Add(leftCell);

        //Add bottom cell
        var botCell = GetCellFromCoordinates(ce.coordinates.x, ce.coordinates.y - 1, true);
        if (botCell != null && excludeContent == null || (excludeContent != null && !excludeContent.Contains(botCell.content))) neighbors.Add(botCell);

        //Add right cell
        var rightCell = GetCellFromCoordinates(ce.coordinates.x + 1, ce.coordinates.y, true);
        if (rightCell != null && excludeContent == null || (excludeContent != null && !excludeContent.Contains(rightCell.content))) neighbors.Add(rightCell);

        return neighbors;
    }

    /// <summary>
    /// Returns how many cells are between cellA and cellB.
    /// </summary>
    /// <param name="cellA"></param>
    /// <param name="cellB"></param>
    /// <returns></returns>
    public int GetCellDistance(Cell cellA, Cell cellB)
    {
        //Since this implementation of A* doesn't take diagonals into account,
        //we can just use a simple Manhattan distance formula instead.
        return Mathf.Abs(cellA.coordinates.x - cellB.coordinates.x) + Mathf.Abs(cellA.coordinates.y - cellB.coordinates.y);
    }
    #endregion
}