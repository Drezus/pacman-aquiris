﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void SelectStage(int st)
    {
        GlobalVariables.selectedStage = st;
        SceneManager.LoadScene(1);
    }
}
