﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    /// <summary>
    /// Ghosts personalities that can be spawned in the game.
    /// </summary>
    public GhostType[] ghostTypes;

    /// <summary>
    /// References spawned ghosts's state machines, so the game can control their behaviour depending on the current ghost phase.
    /// </summary>
    [HideInInspector]
    public List<GhostMovement> spawnedGhosts;

    /// <summary>
    /// Keeps track of the game's current state.
    /// </summary>
    [HideInInspector]
    public GameStates currentGameState;

    /// <summary>
    /// A coroutine delegate that runs the ghosts's states in a finite state machine.
    /// </summary>
    public Coroutine ghostsState;

    /// <summary>
    /// A coroutine that waits some time before spawning a mega-pellet.
    /// </summary>
    public Coroutine megaPelletCoroutine;

    /// <summary>
    /// How many seconds ghosts initially spend on Scatter mode.
    /// "Initially" because scatter mode time shortens by 1 second every time in order 
    /// to make the game progressively harder.
    /// </summary>
    [Header("Game Variables")]
    public float startingScatterDuration = 6f;
    private float currentScatterDuration;
    /// <summary>
    /// How many seconds ghosts spend on Chase mode before going back to Scatter mode.
    /// </summary>
    public float chaseDuration = 20f;
    /// <summary>
    /// How many seconds PAC-MAN stays powered-up and ghosts stay turned-to-blue after eating a power-pellet or mega-pellet.
    /// </summary>
    public float powerupDuration = 6f;

    /// <summary>
    /// How many seconds it takes for a Mega-Pellet to appear.
    /// </summary>
    public float megaPelletAppear = 15f;


    //Keeps track of how many pellets PAC-MAN collected.
    private int pelletsCollected = 0;

    public void Awake()
    {
        spawnedGhosts = new List<GhostMovement>();
        pelletsCollected = 0;

        MazeManager.Instance.CreateMaze(GlobalVariables.selectedStage);

        ///Initializes all spawned ghosts from MazeManager
        for (int i = 0; i < spawnedGhosts.Count; i++)
        {
            //Cycles between all available ghost types from GameManager.
            spawnedGhosts[i].Initialize(
                ghostTypes[i % ghostTypes.Length],
                GetScatterSpot(i));
        }

        StartGame();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (megaPelletCoroutine != null) StopCoroutine(megaPelletCoroutine);
            if (ghostsState != null) StopCoroutine(ghostsState);
            SceneManager.LoadScene(0);
        }
    }

    private void StartGame()
    {
        currentGameState = GameStates.STARTED;
        StartCoroutine(GameStartJingle(4f));
    }

    private IEnumerator GameStartJingle(float waitTime)
    {
        AudioManager.Instance.PlaySound(Sounds.GAME_START);

        yield return new WaitForSeconds(waitTime);
        currentGameState = GameStates.PLAYING;

        currentScatterDuration = startingScatterDuration;

        ghostsState = StartCoroutine(Scatter(startingScatterDuration));

        megaPelletCoroutine = StartCoroutine(MegaPelletTimer(megaPelletAppear));
    }

    private IEnumerator MegaPelletTimer(float duration)
    {
        yield return new WaitForSeconds(duration);

        if (!FindObjectOfType<PlayerMovement>().megaPacman) MazeManager.Instance.SpawnMegaPellet();

        megaPelletCoroutine = StartCoroutine(MegaPelletTimer(megaPelletAppear));
    }

    private IEnumerator DeadJingle(float waitTime)
    {
        currentGameState = GameStates.DEAD;
        AudioManager.Instance.PlaySound(Sounds.DEATH);
        yield return new WaitForSeconds(waitTime);

        ResetPositions();
        StartGame();
    }

    private IEnumerator GameEndJingle(float waitTime)
    {
        StopCoroutine(megaPelletCoroutine);

        currentGameState = GameStates.GAME_OVER;
        AudioManager.Instance.PlaySound(Sounds.GAME_OVER);
        yield return new WaitForSeconds(waitTime);

        SceneManager.LoadScene(0);
    }

    public void GhostEaten(GhostMovement gho)
    {
        gho.GetEaten();
        StartCoroutine(PauseEffect(0.5f, Sounds.GHOST_EATEN));
    }

    public IEnumerator PauseEffect(float waitTime, Sounds? sound = null, Action callback = null)
    {
        currentGameState = GameStates.PAUSED;
        if (sound.HasValue) AudioManager.Instance.PlaySound(sound.Value);

        Time.timeScale = 0f;

        yield return new WaitForSecondsRealtime(waitTime);
        currentGameState = GameStates.PLAYING;

        Time.timeScale = 1f;

        callback?.Invoke();
    }

    private void ResetPositions()
    {
        var objs = FindObjectsOfType<GridMovement>();

        foreach (GridMovement obj in objs)
        {
            obj.ResetPosition();
        }
    }

    #region GhostStates
    private IEnumerator Scatter(float duration)
    {
        foreach (GhostMovement gho in spawnedGhosts)
        {
            gho.Scatter();
        }

        yield return new WaitForSeconds(duration);

        //Reduces scatter time each time it ends.
        currentScatterDuration = Mathf.Clamp(currentScatterDuration - 1, 0, startingScatterDuration);

        StopCoroutine(ghostsState);
        ghostsState = StartCoroutine(Chase(chaseDuration));
    }

    private IEnumerator Chase(float duration)
    {
        foreach (GhostMovement gho in spawnedGhosts)
        {
            gho.Chase();
        }

        yield return new WaitForSeconds(duration);

        StopCoroutine(ghostsState);
        ghostsState = StartCoroutine(Scatter(currentScatterDuration));
    }

    private IEnumerator TurnToBlue(float duration)
    {
        foreach (GhostMovement gho in spawnedGhosts)
        {
            gho.TurnToBlue();
        }

        yield return new WaitForSeconds(duration);

        //Reverts PAC-MAN to regular form if he's MEGA-PAC-MAN.
        PlayerMovement pac = FindObjectOfType<PlayerMovement>();
        if (pac.megaPacman) pac.RevertToRegular();

        if (megaPelletCoroutine == null) megaPelletCoroutine = StartCoroutine(MegaPelletTimer(megaPelletAppear));

        ghostsState = StartCoroutine(Chase(chaseDuration));
    }
    #endregion

    public void PelletCollected(CellContents type)
    {
        if (type != CellContents.PELLET && type != CellContents.POWER_PELLET && type != CellContents.MEGA_PELLET)
            return;

        if (type != CellContents.MEGA_PELLET)
        {
            AudioManager.Instance.PlayMunchingSound();
            pelletsCollected++;
        }

        if (pelletsCollected >= MazeManager.Instance.pelletsParsed)
        {
            StopCoroutine(ghostsState);
            StartCoroutine(GameEndJingle(5f));
            return;
        }

        if (type != CellContents.PELLET)
        {
            ///Power-Pellets don't activate a TURN-TO-BLUE if PAC-MAN is mega.
            PlayerMovement pac = FindObjectOfType<PlayerMovement>();
            if (!pac.megaPacman)
            {
                StopCoroutine(ghostsState);
                ghostsState = StartCoroutine(TurnToBlue(powerupDuration));
            }

            if (type == CellContents.MEGA_PELLET)
            {
                StopCoroutine(megaPelletCoroutine);
                megaPelletCoroutine = null;
                FindObjectOfType<PlayerMovement>().BecomeMega();
            }
        }
    }

    public void LoseLife()
    {
        if (currentGameState != GameStates.DEAD)
        {
            StopCoroutine(ghostsState);
            StopCoroutine(megaPelletCoroutine);
            ghostsState = StartCoroutine(DeadJingle(1.8f));
        }
    }

    private Cell GetScatterSpot(int g)
    {
        //Prevents divided-by-zero errors.
        if (MazeManager.Instance.scatterSpots == null || MazeManager.Instance.scatterSpots.Count <= 0)
        {
            return null;
        }
        else
        {
            return MazeManager.Instance.scatterSpots[g % MazeManager.Instance.scatterSpots.Count];
        }
    }
}

public enum GameStates
{
    STARTED,
    PLAYING,
    PAUSED,
    DEAD,
    GAME_OVER
}
