﻿using UnityEngine;
/// <summary>
/// Static class that stores (usually readonly) variables statically for convenience.
/// </summary>
public static class GlobalVariables
{
    public static int selectedStage = 0;

    /// <summary>
    /// Provides a fixed base speed (PAC-MAN's 100% speed) to be modified by multipliers.
    /// </summary>
    public static readonly float baseSpeed = 0.7f;

    /// <summary>
    /// Provides a fixed base speed for ghosts when they've been eaten and are returning to their spawn point.
    /// </summary>
    public static readonly float eatenSpeed = 2f;

    /// <summary>
    /// Determines the tolerance threshold that a position is considered "close enough" to a cell's origin.
    /// Increasing this value too much makes PAC-MAN cornering quicker but may make it look like it's teleporting.
    /// Decreasing this value too much makes late turn commands harder to input. 
    /// </summary>
    public static readonly float cellCenterThreshold = 0.01f;

    #region Built-in Mazes
    /// <summary>
    /// Captions:
    /// E = Empty
    /// W = Wall
    /// P = PAC-MAN's spawn point. Also serves as a bonus fruit spawn point, when needed.
    /// G = Ghost spawn points. One ghost spawns for each.
    /// D = Ghost-door. Walls that ghosts can go through, but PAC-MAN can't.
    /// . = Regular pellet.
    /// O = Power-Pellet. 
    /// S = Ghost scatter spot. Ghosts share the same spot if the map has more Ghosts than Scatter spots.
    /// Z = Ghost scatter spot with a pellet on it.
    /// X = Ghost house's walls. They can't be destroyed even by MEGA-PAC-MAN.
    /// 
    /// Array Indexes:
    /// 0 - Debug/Test map
    /// 1 - Original Pac-man maze, with 4 ghosts
    /// 2 - Prototype/Beta Pac-man maze, with additional 4 ghost houses with 2 ghosts in each.
    /// </summary>

    public static readonly string[] maps = new string[]
    {
            "WWWWWWWWWWWWWWWWWWWWWWWWWWWW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEWEEESSSSSSSSSSSSEEEEEEEWEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEPEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEO.......EEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEWEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEWEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEWEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEWEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEWEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEWEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEWWWWWWWWWEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW"+
            "WEEGEGEGEGEGEGEGEGEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WEEEEEEEEEEEEEEEEEEEEEEEEEEW" +
            "WWWWWWWWWWWWWWWWWWWWWWWWWWWW",

            "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEWWWWWWWWWWWWWWWWWWWWWWWWWWWWWZ...........WW...........ZWW.WWWW.WWWWW.WW.WWWWW.WWWW.WWOWWWW.WWWWW.WW.WWWWW.WWWWOWW.WWWW.WWWWW.WW.WWWWW.WWWW.WW..........................WW.WWWW.WW.WWWWWWWW.WW.WWWW.WW.WWWW.WW.WWWWWWWW.WW.WWWW.WW......WW....WW....WW......WWWWWWW.WWWWW.WW.WWWWW.WWWWWWEEEEEW.WWWWW.WW.WWWWW.WEEEEEEEEEEW.WWEEEEEEEEEEWW.WEEEEEEEEEEW.WWEXXXDDXXXEWW.WEEEEEWWWWWW.WWEXEEEEEEXEWW.WWWWWWEEEEEE.EEEXEGGGGEXEEE.EEEEEEWWWWWW.WWEXEEEEEEXEWW.WWWWWWEEEEEW.WWEXXXXXXXXEWW.WEEEEEEEEEEW.WWEEEEEEEEEEWW.WEEEEEEEEEEW.WWEWWWWWWWWEWW.WEEEEEWWWWWW.WWEWWWWWWWWEWW.WWWWWWW............WW............WW.WWWW.WWWWW.WW.WWWWW.WWWW.WW.WWWW.WWWWW.WW.WWWWW.WWWW.WWO..WW.......PE.......WW..OWWWW.WW.WW.WWWWWWWW.WW.WW.WWWWWW.WW.WW.WWWWWWWW.WW.WW.WWWW......WW....WW....WW......WW.WWWWWWWWWW.WW.WWWWWWWWWW.WW.WWWWWWWWWW.WW.WWWWWWWWWW.WWZ........................ZWWWWWWWWWWWWWWWWWWWWWWWWWWWWWEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE",

            "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEXXXXXWWWWWWWWWWWWWWXXXXXEEEEXEGEX...Z......Z...XEGEXEEEEXEEED.WW.WWWWWW.WW.DEEEEEEEEXEGEX.WW.WWWWWW.WW.XEGEXEEEEXXXXX.WW...WW...WW.XXXXXEEEEEEEEW.WWWW.WW.WWWW.WEEEEEEWWWWWWW.WWWW.WW.WWWW.WWWWWWW...O....WW........WW....O...WWW.WWWWWW.WWWWWW.WWWWWW.WWWWWW.WWWWWW.WWWWWW.WWWWWW.WWWW.......WW...WW...WW.......WW.WWWWW.WWWW.WW.WWWW.WWWWW.WW.WWWWW.WWWW.WW.WWWW.WWWWW.WW....WWZ............ZWW....WWWWW.WWWW.WWWEEWWW.WWWW.WWWWWWWW.WWWW.WEEEEEEW.WWWW.WWWW..WW......EEEPEEEE......WW..W.WW.WWWW.WEEEEEEW.WWWW.WW.WW....WWWW.WWWEEWWW.WWWW....WW.WW.WW..............WW.WW.WW.WW.WW.WWWWWWWWWWWW.WW.WW.WW.WW.WW.WWWWWWWWWWWW.WW.WW.W..WW.WW......WW......WW.WW..WWWW.WWWWWWW.WW.WWWWWWW.WWWWWWWW.WWWWWWW.WW.WWWWWWW.WWWWW...Z..................Z...WW.WW.XXXXXWWWWWWWWXXXXX.WW.WWOWW.XEGEXEEEEEEEEXEGEX.WWOWW.WW.DEEEXEEEEEEEEXEEED.WW.WW.WW.XEGEXEEEEEEEEXEGEX.WW.WW....XXXXXEEEEEEEEXXXXX....WWWWWWWEEEEEEEEEEEEEEEEWWWWWWEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
    };
    #endregion
}