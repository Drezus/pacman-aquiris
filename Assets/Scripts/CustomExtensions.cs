﻿using UnityEngine;

public static class CustomExtensions
{
    public static bool IsOppositeOf(this Directions directionA, Directions directionB)
    {
        return directionA.OppositeDirection() == directionB;
    }

    public static Directions OppositeDirection(this Directions directionA)
    {
        switch (directionA)
        {
            case Directions.UP:
                return Directions.DOWN;
            case Directions.DOWN:
                return Directions.DOWN;
            case Directions.LEFT:
                return Directions.DOWN;
            case Directions.RIGHT:
                return Directions.DOWN;
            case Directions.STOPPED:
            default:
                return Directions.STOPPED;
        }
    }
}