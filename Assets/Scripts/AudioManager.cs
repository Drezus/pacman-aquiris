﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum Sounds
{
    GAME_START,
    GAME_OVER,
    DEATH,
    GHOST_EATEN,
    DESTROY,
    MEGA_PELLET_APPEAR,
    MEGA_PELLET_EATEN,
    MEGA_PELLET_OVER
}

/// <summary>
/// Using custom structs so Unity may render them in the inspector as lists with dropdowns.
/// </summary>
[Serializable]
public struct SoundEffect
{
    public AudioClip clip;
    public Sounds name;
}

public class AudioManager : Singleton<AudioManager>
{
    public AudioSource source;

    /// <summary>
    /// Sounds effects are played using their corresponding Enum names.
    /// </summary>
    public List<SoundEffect> soundEffects;

    /// <summary>
    /// The original PAC-MAN has two munching sounds, which together make the famous "wakka" sound.
    /// This variable keeps track of the last munching sound used so they make the wakka-wakka sound sequence.
    /// </summary>
    [Header("Munching Sounds")]
    public AudioClip wa;
    public AudioClip kka;
    bool wakka = true;

    public void PlaySound(Sounds so)
    {
        SoundEffect soundEffect = soundEffects.FirstOrDefault(s => s.name == so);
        source.PlayOneShot(soundEffect.clip);
    }

    public void PlayMunchingSound()
    {
        source.PlayOneShot(wakka ? wa : kka);
        wakka = !wakka;
    }
}
